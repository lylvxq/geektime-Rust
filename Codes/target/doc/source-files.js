var N = null;var sourcesIndex = {};
sourcesIndex["inviting_rust"] = {"name":"","dirs":[{"name":"ch01","files":["ctfe.rs","expr.rs","lexical.rs","mod.rs"]},{"name":"ch02","files":["mod.rs","s1_ownership.rs","s2_lifetime.rs","s3_thread_safe.rs","s4_lockfree.rs","s5_trait_and_generic.rs","s6_paradigms.rs","s7_error_handle.rs","s8_metaprogramming.rs","s9_unsafe_rust.rs"]},{"name":"ch03","files":["mod.rs","s1_io_model.rs","s2_async_await.rs","s3_async_runtime.rs"]}],"files":["lib.rs"]};
createSourceSidebar();
